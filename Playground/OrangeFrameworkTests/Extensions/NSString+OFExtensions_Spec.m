@import Quick;
@import Nimble;
@import OrangeFramework;

QuickSpecBegin(OFExtensions_NSString_Spec)

describe(@"NSString+Regex", ^{
  context(@"when the string contains regex pattern", ^{
    NSString *testString = @"This is test string.";
    it(@"match regex", ^{
      NSString *testRegex = @".*";
      expect(@([testString of_matchRegex:testRegex])).to(beTrue());
    });
    
    it(@"has first match of regex", ^{
      NSString *testRegex = @"is";
      expect([testString of_firstMatchOfRegex:testRegex]).toNot(beNil());
    });
    
    it(@"has all matches of regex", ^{
      NSString *testRegex = @"is";
      NSInteger resultCount = 2;
      expect(@([testString of_allMatchesOfRegex:testRegex].count)).to(equal(@(resultCount)));
    });
    
    it(@"has match email", ^{
      NSString *testEmail = @"test@mail.com";
      expect(@([testEmail of_matchEmailPattern])).to(beTrue());
    });
  });
  context(@"when the string NOT contains regex pattern", ^{
    NSString *testString = @"This is test string.";
    it(@"not match regex", ^{
      NSString *testRegex = @"This";
      expect(@([testString of_matchRegex:testRegex])).toNot(beTrue());
    });
    it(@"has not first match of regex", ^{
      NSString *testRegex = @"testing";
      expect([testString of_firstMatchOfRegex:testRegex]).to(beNil());
    });
    
    it(@"has not all matches of regex", ^{
      NSString *testRegex = @"testing";
      expect([testString of_firstMatchOfRegex:testRegex]).to(beNil());
    });
    
    it(@"has not match email", ^{
      NSString *testEmail = @"testmail.com";
      expect(@([testEmail of_matchEmailPattern])).toNot(beTrue());
    });
  });
});

QuickSpecEnd

