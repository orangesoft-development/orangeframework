@import Quick;
@import Nimble;
@import OrangeFramework;

QuickSpecBegin(OFExtensions_NSBlockOperation_Spec)

describe(@"NSBlockOperation+AsyncBlock", ^{
  context(@"when use serial queue", ^{
    it(@"run async operation in series", ^{
      __block NSString *testString = @"";
      NSOperationQueue *testOperationQueue = [NSOperationQueue new];
      testOperationQueue.maxConcurrentOperationCount = 1;
      [testOperationQueue of_addOperationWithAsyncBlock:^(OFAsyncBlockFinish  _Nonnull finish) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          testString = [testString stringByAppendingString:@"1"];
          finish();
        });
      }];
      [testOperationQueue of_addOperationWithAsyncBlock:^(OFAsyncBlockFinish  _Nonnull finish) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          testString = [testString stringByAppendingString:@"2"];
          finish();
        });
      }];
      [testOperationQueue of_addOperationWithAsyncBlock:^(OFAsyncBlockFinish  _Nonnull finish) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          testString = [testString stringByAppendingString:@"3"];
          finish();
        });
      }];
      expect(testString).toEventually(equal(@"123"));
    });
  });
  context(@"when use parallel queue", ^{
    it(@"run async operation in parallel", ^{
      __block NSString *testString = @"";
      NSOperationQueue *testOperationQueue = [NSOperationQueue new];
      testOperationQueue.maxConcurrentOperationCount = 3;
      [testOperationQueue of_addOperationWithAsyncBlock:^(OFAsyncBlockFinish  _Nonnull finish) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          testString = [testString stringByAppendingString:@"1"];
          finish();
        });
      }];
      [testOperationQueue of_addOperationWithAsyncBlock:^(OFAsyncBlockFinish  _Nonnull finish) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          testString = [testString stringByAppendingString:@"2"];
          finish();
        });
      }];
      [testOperationQueue of_addOperationWithAsyncBlock:^(OFAsyncBlockFinish  _Nonnull finish) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
          testString = [testString stringByAppendingString:@"3"];
          finish();
        });
      }];
      expect(testString).toEventually(equal(@"132"));
    });
  });
});

QuickSpecEnd

