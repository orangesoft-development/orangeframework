//: Playground - noun: a place where people can play

import UIKit
import OrangeFramework

// Array

let element = [3, 4, 5].of_safeElement(at: 2)
let nilElement = [1, 2, 3].of_safeElement(at: 3)

var array = [1, 2, 1, 3]
array.of_remove(1)

/// Date

var date = Date(of_string: "23.12.2016 12:23", format: "dd.MM.yyyy hh:mm")
date = Date(of_string: "23.12.2016 12:23", format: "dd.MM.yyyy hh:mm", timezone: TimeZone(identifier: "UTC"))
date = Date(of_components: DateComponents())

var components = Date().of_components()

let changedDate = Date().of_changed { components in
  components.day? += 1
  components.minute? -= 35
}

var stringDate = Date().of_toString(format: "dd.MM.yyyy HH:mm")
stringDate = Date().of_toString(formatTemplate: "ddMMyyyyHHmm")
stringDate = Date().of_toString(format: "dd.MM.yyyy hh:mm", timezone: TimeZone(identifier: "UTC"))
stringDate = Date().of_toString(dateStyle: .medium, timeStyle: .medium)
stringDate = Date().of_toString(dateStyle: .long, timeStyle: .long, timezone: TimeZone(identifier: "UTC"))

var isEqual = Date().of_isEqualIgnoringTime(to: Date(timeIntervalSinceNow: 100))
isEqual = Date().of_isEqualIgnoringTime(to: Date(timeIntervalSinceNow: 24*60*60))

// Codable & JSONDecoder & JSONEncoder

struct Person: Codable {
    struct Name: Codable {
        let first: String
        let last: String
    }
    let isActive: Bool
    let age: Int
    let name: Name
    let email: String
}

let person = Person(isActive: true, age: 30, name: Person.Name(first: "Orange", last: "Framework"), email: "orangeframework@gmail.com")

do {
    let json = try person.encode()
    let decodedPerson = try Person.decode(json: json)
    String(describing: decodedPerson)
    
    //for custom JSONEncoder or JSONDecoder
    
    let jsonWithEncoder = try JSONEncoder().encodeToJSON(person)
    let decodedPersonWithDecoder = try JSONDecoder().decode(Person.self, fromJSON: jsonWithEncoder)
    String(describing: decodedPersonWithDecoder)
} catch {
    String(describing: error)
}


// String

"2016".of_toInt()
"999.9".of_toDouble()
"999,9".of_toDouble()
"hello, world".of_readingTime()
"hello, world".of_readingTime(speed: 150)

"hello, world".of_match(regex: "hello,.*")
"email@gmail.com".of_matchEmailPattern()
"\\"

"Hello, world! Hello, swift!".of_firstMatch(ofRegex: "Hello,\\s*(\\w+)")
"Hello, world! Hello, swift!".of_allMatches(ofRegex: "Hello,\\s*(\\w+)")

"  password ".of_removeSpaces()

String(of_int: 99999999999, groupingSeparator: ",")

// Localization

"Localized string with parameters %d, %d".of_localized(1, 2)

/// Error

// Вывести сообщение в лог, по-умолчанию уровень - .default
of_print("Message with Default level")
of_print("Message with Error level", level: .error)

// Перехватить логи
of_printFilter { (formattedMessage, rawMessage, level, function, file, line) -> Bool in
  if level == .error {
    // отправить в crashlytics, например
  }
  return true
}

/// Error

// Конструкторы для NSError
NSError(of_domain: "domain", code: 404, description: "Error description")
NSError(of_domainObject: NSObject(), description: "Error description")
NSError(of_domainObject: NSObject(), code: 404, description: "Error description")

/// GCD

// Выполнить код асинхронно в главном потоке
of_dispatch_main {
  // code
}

// Выполнить код асинхронно в фоновом потоке
of_dispatch_background {
  // code
}

// Выполнить код асинхронно в главном потоке через 10 секунд
of_dispatch_main_after(10) {
  // code
}

// Выполнить код асинхронно в фоновом потоке через 10 секунд
of_dispatch_background_after(10) {
  // code
}

/// NSOperation

// Создание BlockOperation с ассинхронным блоком кода
var asyncBlock = BlockOperation(of_asyncBlock: { finish in
  // code
  finish()
})

asyncBlock = BlockOperation()
asyncBlock.of_addExecutionAsyncBlock { finish in
  // code
  finish()
}

// Добавить асинхронный блок кода в очередь
let queue = OperationQueue()
queue.addOperation {
  // code
}
queue.of_addOperation { finish in
  // code
  finish()
}


/// Random

// Cлучайное булевое значение
of_random_bool()

// Случайное значение int, double в заданном диапазоне
of_random_int(0, 9)
of_random_double(0.1, 0.9)

/// Warning

// Добавить FIXME напоминание в Issue Navigator
of_warning() // <Описание>

/// UIView

let view = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
// Радиус улов view(IBInspectable)
view.of_cornerRadius = 15

// Границы view (IBInspectable)
view.of_borderWidth = 2
view.of_borderColor = UIColor.black

// Добавить разделитель внизу view (IBInspectable)
view.of_separatorColor = UIColor.red
view.of_separatorLeftInset = 5.0
view.of_separatorRightInset = 5.0


/// NSAttributedString

// Создание строки с атрибутами
let string = NSMutableAttributedString(of_string: "attributed", with: [OFStringAttribute.foregroundColor(.black), OFStringAttribute.backgroundColor(.green)])

// Добавить строку атрибутами
string.of_appendString("string", with: [OFStringAttribute.underlineStyle(2), OFStringAttribute.stroke(.red), OFStringAttribute.strokeWidth(1)])
string.of_appendString("степень", with: [OFStringAttribute.baselineOffset(10)])

// Добавить атрибуты к строке
string.of_addAtributes([.font(UIFont(name: "Helvetica Neue", size: 20)!)])

/// NSLayoutConstraint

NSLayoutConstraint().of_constantInPixels = 1


/// UIColor

// Инициализаторы UIColor
UIColor(of_hexString: "ff0000")
UIColor(of_hexString: "ff0000", alpha: 0.5)
UIColor(255, 0, 0)
UIColor(255, 0, 0, 0.5)


// UIImage

// Создание UIImage заданного цвета
var image = UIImage(of_color: .red, size: CGSize(width: 100, height: 100))
image = UIImage(of_color: .red, size: CGSize(width: 100, height: 100), transparentInsets: UIEdgeInsets(top: 15, left: 0, bottom: 15, right: 0))

// Создание UIImage из view
image = UIImage(of_view: view)

// Скруглить углы изображения
var newImage = image.of_imageRounded(withCornerRadius: 15.0)
newImage = image.of_imageRoundedIntoCircle()

// Масштабирование изображения
newImage = image.of_imageScaled(to: CGSize(width: 200, height: 250))
newImage = image.of_imageScaled(toFit: CGSize(width: 200, height: 200))
newImage = image.of_imageScaled(toFill: CGSize(width: 200, height: 200))

// Исправить ориентацию изображения
newImage = image.of_imageWithNormalizedOrientation()

// Проверка изображения на непрозрачность
image.of_isOpaque

// UINavigationController

let navigationController = UINavigationController()

// Установить фоновое изображения для навбара
navigationController.of_navBarImage = image

// Вкл/выкл тень навбара
navigationController.of_navBarShadow = false

// Вкл/выкл прозрачность навбара
navigationController.of_navBarTransparent = false

// Установить фоновое изображения
navigationController.of_backgroundImage = image

// Вкл/выкл анимацию затухания при переходах
navigationController.of_fadeAnimation = true

// Вкл/выкл анимацию слайда при переходах
navigationController.of_slideAnimation = true

// Вкл/выкл pop-переход свайпом
navigationController.of_popGesture = true


// UIViewController

let viewController = UIViewController()

// Получить верхний UIViewController
let topViewController = UIViewController.of_top()

// Показан ли viewController на экране
viewController.of_appearing

// Скрыт ли viewController с экрана
viewController.of_disappearing

//
viewController.of_popAction(nil)
viewController.of_dismissAction(nil)

// Переход по segue с блоком кода `prepareForSegue`
//viewController.of_performSegue(withIdentifier: "segue") { segue in
//  // prepareForSegue code
//}

// UITextField

let textField = UITextField()
let barButtonItem = UIBarButtonItem()

textField.placeholder = "placeholder"

// Устанавливает цвет текста placeholder
textField.of_placeholderColor = .red

// Добавляет кнопки в inputAccessoryView
textField.of_inputAccessoryLeftButton = barButtonItem
textField.of_inputAccessoryRightButton = barButtonItem

// Задаёт смещение теста внутри UITextField
textField.of_textInsets = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)


// Dependency

class SomeClass {
  
}

// Зарегестрировать объект в менеджере зависимостей
Dependency.register(SomeClass.self, lifetime: .singleton) { SomeClass() }

// Проверить наличие объекта
Dependency.available(SomeClass.self)

// Получить объект
var object: SomeClass = Dependency.resolve(SomeClass.self)

// Удалить объект из менеджера
Dependency.remove(SomeClass.self)

DependencyLifetime.singleton

//Dependency.register(byKey: "key", lifetime: .weakSingleton) { SomeClass(value: 2) }
//Dependency.remove(forKey: "key")
//Dependency.available(forKey: "key")
//object = Dependency.resolve()
//var object1 = Dependency.resolve(byKey: "key")

// Device

Device.screenScale
Device.screenWidth
Device.screenHeight
Device.screenMaxLength
Device.screenMinLength
Device.screenPixelLength

Device.heightForStatusBar
Device.heightForNavBarPortrate
Device.heightForNavBarLandscape
Device.heightForTabBar

Device.isScreen3Dot5inch
Device.isScreen4inch
Device.isScreen4Dot7inch
Device.isScreen5Dot5inch

Device.isIPhone
Device.isIPad

Device.deviceModel

Device.pathToDocuments
Device.pathToCaches

Device.systemVersionEqual(to: "10.2")
Device.systemVersionLessThan("11")
Device.systemVersionGreaterThan("8.0")


var pointer: Pointer<[String]> = Pointer(value: ["213"])
