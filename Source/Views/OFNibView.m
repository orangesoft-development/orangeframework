#import "OFNibView.h"

@interface OFNibView ()
@property (nonatomic, strong, readwrite) NSString *nibName;
@property (nonatomic, strong, readwrite) UIView *contentView;
@end

@implementation OFNibView

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self commonSetup];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  return [super initWithCoder:coder];
}

- (instancetype)initWithNibName:(NSString *)nibName {
  self = [super initWithFrame:CGRectZero];
  if (self) {
    self.nibName = nibName;
    [self commonSetup];
  }
  return self;
}

- (void)awakeFromNib {
  [super awakeFromNib];
  [self commonSetup];
}

- (void)setIntrinsicContentSize:(CGSize)intrinsicContentSize {
  _intrinsicContentSize = intrinsicContentSize;
  [self invalidateIntrinsicContentSize];
}

- (void)commonSetup {
  if (!self.nibName) {
    NSString *className = NSStringFromClass([self class]);
    NSString *swiftClassName = className.pathExtension;
    self.nibName = [swiftClassName isEqualToString:@""] ? className : swiftClassName;
  }
  self.contentView = [[[NSBundle mainBundle] loadNibNamed:self.nibName owner:self options:nil] firstObject];
  _intrinsicContentSize = self.contentView.bounds.size;
  [self addSubview:self.contentView];
  // bind contentView to self
  self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
  NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1 constant:0];
  NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:0];
  NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
  NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:self attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
  [self addConstraints:@[leftConstraint, topConstraint, widthConstraint, heightConstraint]];
}

@end
