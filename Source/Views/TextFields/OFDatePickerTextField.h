#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OFDatePickerTextField : UITextField

@property (nonatomic, strong, readonly) UIDatePicker *datePicker;
@property (nonatomic, strong, nullable) IBInspectable UIColor *pickerTextColor;
@property (nonatomic, strong, nullable) NSDateFormatter *dateFormatter; // uses internal date formatter if nil
@property (nonatomic, strong) NSDate *date;
@property (nonatomic) NSTimeInterval countDownDuration; // for UIDatePickerModeCountDownTimer

- (void)setDate:(NSDate *)date animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
