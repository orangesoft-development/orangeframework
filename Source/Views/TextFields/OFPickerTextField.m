#import "OFPickerTextField.h"

@interface TranslucentPickerView : UIPickerView
@end

@implementation TranslucentPickerView
- (void)layoutSubviews {
  [super layoutSubviews];
  self.backgroundColor = [UIColor clearColor];
}
@end

@interface OFPickerTextField () <UIPickerViewDataSource, UIPickerViewDelegate>
@end

@implementation OFPickerTextField

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self commonSetup];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    [self commonSetup];
  }
  return self;
}

- (void)setItems:(NSArray<NSString *> *)items {
  _items = items;
  [self.pickerView reloadAllComponents];
}

- (void)setSelectedItemIndex:(NSInteger)index {
  _selectedItemIndex = index;
  [self.pickerView selectRow:index inComponent:0 animated:NO];
  self.text = self.items[index];
}

- (void)selectItemAtIndex:(NSInteger)index animated:(BOOL)animated {
  _selectedItemIndex = index;
  [self.pickerView selectRow:index inComponent:0 animated:animated];
  self.text = self.items[index];
}

#pragma mark Internal Helpers

- (void)commonSetup {
  self.items = [NSArray new];
  _selectedItemIndex = -1;
  _pickerView = [TranslucentPickerView new];
  self.pickerView.delegate = self;
  self.pickerView.dataSource = self;
  self.inputView = self.pickerView;
}

- (BOOL)becomeFirstResponder {
  BOOL result = [super becomeFirstResponder];
  if (result && self.items.count > 0 && self.selectedItemIndex < 0) {
    self.selectedItemIndex = 0;
  }
  return result;
}

#pragma mark UIPickerViewDataSource & UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
  return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
  return self.items.count;
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
  NSDictionary *attr = self.pickerTextColor ? @{NSForegroundColorAttributeName: self.pickerTextColor} : nil;
  return [[NSAttributedString alloc] initWithString:self.items[row] attributes:attr];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
  self.text = self.items[row];
}

@end
