#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OFPickerTextField : UITextField

@property (nonatomic, strong, readonly) UIPickerView *pickerView;
@property (nonatomic, strong, nullable) IBInspectable UIColor *pickerTextColor;
@property (nonatomic, strong) NSArray<NSString *> *items;
@property (nonatomic) NSInteger selectedItemIndex; // default is -1

- (void)selectItemAtIndex:(NSInteger)index animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
