#import "OFDatePickerTextField.h"

@interface TranslucentDatePicker : UIDatePicker
@property (nonatomic, strong, nullable) UIColor *of_textColor;
@end

@implementation TranslucentDatePicker

- (UIColor *)of_textColor {
  return [super valueForKey:@"textColor"];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  self.backgroundColor = [UIColor clearColor];
  if (self.of_textColor != _of_textColor) {
    [self setValue:_of_textColor forKey:@"textColor"];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self performSelector:NSSelectorFromString(@"setHighlightsToday:") withObject:_of_textColor];
#pragma clang diagnostic pop
  }
}

@end

@interface OFDatePickerTextField () {
  TranslucentDatePicker *_datePicker;
}
@end

@implementation OFDatePickerTextField

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self commonSetup];
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
  self = [super initWithCoder:coder];
  if (self) {
    [self commonSetup];
  }
  return self;
}

- (UIDatePicker *)datePicker {
  return _datePicker;
}

- (void)setPickerTextColor:(UIColor *)pickerTextColor {
  _datePicker.of_textColor = pickerTextColor;
}

- (UIColor *)datePickerTextColor {
  return _datePicker.of_textColor;
}

- (void)setDate:(NSDate *)date {
  _datePicker.date = date;
  [self updateText];
}

- (NSDate *)date {
  return _datePicker.date;
}

- (void)setCountDownDuration:(NSTimeInterval)countDownDuration {
  _datePicker.countDownDuration = countDownDuration;
  [self updateText];
}

- (NSTimeInterval)countDownDuration {
  return _datePicker.countDownDuration;
}

- (void)setDate:(NSDate *)date animated:(BOOL)animated {
  [_datePicker setDate:date animated:animated];
  [self updateText];
}

- (BOOL)becomeFirstResponder {
  BOOL result = [super becomeFirstResponder];
  [self updateText];
  return result;
}

#pragma mark Internal Helpers

- (void)commonSetup {
  _datePicker = [TranslucentDatePicker new];
  [_datePicker addTarget:self action:@selector(updateText) forControlEvents:UIControlEventValueChanged];
  self.inputView = _datePicker;
}

- (void)updateText {
  NSDate *date = self.date;
  NSDateFormatter *defaultDateFormatter = [NSDateFormatter new];
  switch (self.datePicker.datePickerMode) {
    case UIDatePickerModeDate:
      defaultDateFormatter.dateStyle = kCFDateFormatterMediumStyle;
      defaultDateFormatter.timeStyle = kCFDateFormatterNoStyle;
      break;
    case UIDatePickerModeTime:
      defaultDateFormatter.dateStyle = kCFDateFormatterNoStyle;
      defaultDateFormatter.timeStyle = kCFDateFormatterShortStyle;
      break;
    case UIDatePickerModeDateAndTime:
      defaultDateFormatter.dateStyle = kCFDateFormatterMediumStyle;
      defaultDateFormatter.timeStyle = kCFDateFormatterShortStyle;
      break;
    case UIDatePickerModeCountDownTimer:
      date = [NSDate dateWithTimeIntervalSince1970:self.countDownDuration];
      defaultDateFormatter.dateFormat = @"HH:mm";
      defaultDateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
      break;
  }
  self.text = [(self.dateFormatter ?: defaultDateFormatter) stringFromDate:date];
}

@end
