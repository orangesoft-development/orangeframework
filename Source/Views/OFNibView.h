#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/*
  In the specified NIB, the File's Owner proxy should have its class set to your view subclass.
  For change view size you can:
    - Change intrinsicContentSize. It depends on view resistance and hugging priorities. More https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/AutolayoutPG/ViewswithIntrinsicContentSize.html
    - Change constraints for subviews. Must exists constraints to all sides of content view.
    - Change constraints for OFNibView.
*/

@interface OFNibView : UIView

- (instancetype)initWithFrame:(CGRect)frame NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_DESIGNATED_INITIALIZER;
- (instancetype)initWithNibName:(nullable NSString *)nibName NS_DESIGNATED_INITIALIZER; // uses class name if nibName is nil

@property (nonatomic, strong, nullable, readonly) IBInspectable NSString *nibName; // editable from storyboard
@property (nonatomic, strong, readonly) UIView *contentView;
@property (nonatomic) CGSize intrinsicContentSize; // by default uses contentView size

@end

NS_ASSUME_NONNULL_END
