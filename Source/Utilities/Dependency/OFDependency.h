#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, OFDependencyLifetime)
{
  OFDependencyLifetimeTransient,
  OFDependencyLifetimeSingleton,
  OFDependencyLifetimeWeakSingleton
} NS_SWIFT_NAME(DependencyLifetime);

NS_SWIFT_NAME(Dependency)
@interface OFDependency : NSObject 

+ (void)registerByKey:(NSString *)key lifetime:(OFDependencyLifetime)lifetime constructor:(id (^)(void))constructor;
+ (id)resolveByKey:(NSString *)key;
+ (BOOL)availableForKey:(NSString *)key;
+ (void)removeForKey:(NSString *)key;

+ (void)registerByClass:(Class)class lifetime:(OFDependencyLifetime)lifetime constructor:(id (^)(void))constructor NS_SWIFT_UNAVAILABLE("");
+ (id)resolveByClass:(Class)class NS_SWIFT_UNAVAILABLE("");
+ (BOOL)availableForClass:(Class)class NS_SWIFT_UNAVAILABLE("");
+ (void)removeForClass:(Class)class NS_SWIFT_UNAVAILABLE("");

+ (void)registerByProtocol:(Protocol *)protocol lifetime:(OFDependencyLifetime)lifetime constructor:(id (^)(void))constructor NS_SWIFT_UNAVAILABLE("");
+ (id)resolveByProtocol:(Protocol *)protocol NS_SWIFT_UNAVAILABLE("");
+ (BOOL)availableForProtocol:(Protocol *)protocol NS_SWIFT_UNAVAILABLE("");
+ (void)removeForProtocol:(Protocol *)protocol NS_SWIFT_UNAVAILABLE("");

@end

NS_ASSUME_NONNULL_END
