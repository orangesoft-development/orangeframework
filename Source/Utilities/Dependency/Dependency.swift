import Foundation

public extension Dependency {
  
  // MARK: - By Type
  
  class func register<T: Any>(_ type: T.Type, lifetime: DependencyLifetime, constructor: @escaping () -> T) {
    let dependencyKey = String(reflecting: T.self)
    register(byKey: dependencyKey, lifetime: lifetime, constructor: constructor)
  }
  
  class func resolve<T: Any>(_ type: T.Type = T.self) -> T {
    let dependencyKey = String(reflecting: T.self)
    let instance = resolve(byKey: dependencyKey) as? T
    assert(instance != nil, "Resolved object is not kind of type \(dependencyKey)")
    return instance!
  }
  
  class func available<T: Any>(_ type: T.Type) -> Bool {
    let dependencyKey = String(reflecting: T.self)
    return available(forKey: dependencyKey)
  }

  class func remove<T: Any>(_ type: T.Type) {
    let dependencyKey = String(reflecting: T.self)
    return remove(forKey: dependencyKey)
  }

}
