#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(Device)
@interface OFDevice : NSObject

#pragma mark - Screen
  
@property (class, nonatomic, readonly) CGFloat screenScale;
@property (class, nonatomic, readonly) CGFloat screenWidth;
@property (class, nonatomic, readonly) CGFloat screenHeight;
@property (class, nonatomic, readonly) CGFloat screenMaxLength;
@property (class, nonatomic, readonly) CGFloat screenMinLength;
@property (class, nonatomic, readonly) CGFloat screenPixelLength;
  
@property (class, nonatomic, readonly) CGFloat heightForStatusBar;
@property (class, nonatomic, readonly) CGFloat heightForNavBarPortrate;
@property (class, nonatomic, readonly) CGFloat heightForNavBarLandscape;
@property (class, nonatomic, readonly) CGFloat heightForTabBar;
  
@property (class, nonatomic, readonly) BOOL isScreen3Dot5inch;
@property (class, nonatomic, readonly) BOOL isScreen4inch;
@property (class, nonatomic, readonly) BOOL isScreen4Dot7inch;
@property (class, nonatomic, readonly) BOOL isScreen5Dot5inch;

#pragma mark - Model

@property (class, nonatomic, readonly) BOOL isIPhone;
@property (class, nonatomic, readonly) BOOL isIPad;
  
@property (class, nonatomic, strong, readonly) NSString *deviceModel;

#pragma mark - System
  
@property (class, nonatomic, strong, readonly) NSString *pathToDocuments;
@property (class, nonatomic, strong, readonly) NSString *pathToCaches;

+ (BOOL)systemVersionEqualTo:(NSString *)version;
+ (BOOL)systemVersionLessThan:(NSString *)version;
+ (BOOL)systemVersionGreaterThan:(NSString *)version;

@end

NS_ASSUME_NONNULL_END
