#import "OFPointer.h"

NSString *const OFPointerDidChangeNotification = @"OFPointerDidChangeNotification";

@implementation OFPointer

- (instancetype)initWithValue:(id)value {
  self = [super init];
  if (self) {
    self.value = value;
    [[NSNotificationCenter defaultCenter] postNotificationName:OFPointerDidChangeNotification object:self];
  }
  return self;
}

@end
