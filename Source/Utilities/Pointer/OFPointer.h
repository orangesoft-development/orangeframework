#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const OFPointerDidChangeNotification NS_SWIFT_NAME(PointerDidChangeNotification);

NS_SWIFT_UNAVAILABLE("Use Pointer struct instead")
@interface OFPointer<T> : NSObject

- (instancetype)initWithValue:(nullable T)value;
@property (nonatomic, strong, nullable) T value;

@end

NS_ASSUME_NONNULL_END
