#import "UIViewController+OFExtension.h"
#import "NSObject+OFExtension.h"

NSString *const kPreparationBlock = @"of_kPreparationBlock";
NSString *const kAppearingFlag = @"of_kAppearingFlag";
NSString *const kDisappearingFlag = @"of_kDisappearingFlag";

@implementation UIViewController (OFExtension)

+ (void)load {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    [self of_swizzleMethod:@selector(viewWillAppear:) withMethod:@selector(of_viewWillAppear:)];
    [self of_swizzleMethod:@selector(viewDidAppear:) withMethod:@selector(of_viewDidAppear:)];
    [self of_swizzleMethod:@selector(viewWillDisappear:) withMethod:@selector(of_viewWillDisappear:)];
    [self of_swizzleMethod:@selector(viewDidDisappear:) withMethod:@selector(of_viewDidDisappear:)];
    [self of_swizzleMethod:@selector(prepareForSegue:sender:) withMethod:@selector(of_prepareForSegue:sender:)];
  });
}

+ (UIViewController *)of_topViewController {
  UIViewController *topVC = [UIApplication sharedApplication].keyWindow.rootViewController;
  while (topVC.presentedViewController) {
    topVC = topVC.presentedViewController;
  }
  return topVC;
}

- (BOOL)of_appearing {
  return self.of_userInfo[kAppearingFlag] != nil;
}

- (BOOL)of_disappearing {
  return self.of_userInfo[kDisappearingFlag] != nil;
}

- (IBAction)of_popAction:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)of_dismissAction:(id)sender {
  [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)of_performSegueWithIdentifier:(NSString *)identifier preparation:(OFSeguePreparationBlock)preparation {
  self.of_userInfo[kPreparationBlock] = [preparation copy];
  [self performSegueWithIdentifier:identifier sender:nil];
}

#pragma mark Swizzled

- (void)of_viewWillAppear:(BOOL)animated {
  [self of_viewWillAppear:animated]; // call original exchanged method
  self.of_userInfo[kAppearingFlag] = @YES;
}

- (void)of_viewDidAppear:(BOOL)animated {
  [self of_viewDidAppear:animated]; // call original exchanged method
  self.of_userInfo[kAppearingFlag] = nil;
}

- (void)of_viewWillDisappear:(BOOL)animated {
  [self of_viewWillDisappear:animated]; // call original exchanged method
  self.of_userInfo[kDisappearingFlag] = @YES;
}

- (void)of_viewDidDisappear:(BOOL)animated {
  [self of_viewDidDisappear:animated]; // call original exchanged method
  self.of_userInfo[kDisappearingFlag] = nil;
}

- (void)of_prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  OFSeguePreparationBlock preparationBlock = self.of_userInfo[kPreparationBlock];
  if (preparationBlock) {
    preparationBlock(segue);
    self.of_userInfo[kPreparationBlock] = nil;
  }
  [self of_prepareForSegue:segue sender:sender]; // call original exchanged method
}

@end
