#import "UINavigationController+OFExtension.h"
#import "UIViewController+OFExtension.h"
#import "NSObject+OFExtension.h"

// popGesture implementation based on https://github.com/fastred/AHKNavigationController/blob/master/Classes/AHKNavigationController.m

NSString *const kBackgroundImageView = @"of_kBackgroundImageView";
NSString *const kNavigationAnimator = @"of_kNavigationAnimator";
NSString *const kPopGestureEnabled = @"of_kPopGestureEnabled";
NSString *const kSystemPopGestureDelegate = @"of_kSystemPopGestureDelegate";

@interface OFNavigationAnimator () <UIGestureRecognizerDelegate>
@property (nonatomic) UINavigationControllerOperation navigationOperation;
@property (nonatomic, strong) UIGestureRecognizer *interactivePopGestureRecognizer;
@property (nonatomic, strong) UIPercentDrivenInteractiveTransition *interactiveTransition;
@property (nonatomic, weak) UINavigationController *internalInteractivePopGestureOwner;
@end

#pragma mark -

@implementation UINavigationController (OFExtension)

+ (void)load {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    [self of_swizzleMethod:@selector(viewDidLoad) withMethod:@selector(of_viewDidLoad)];
  });
}

#pragma mark Swizzled

- (void)of_viewDidLoad {
  [self of_viewDidLoad]; // call original exchanged method
                         // reset pop gesture
  [self setupPopGesture];
}

#pragma mark Nav Bar

- (void)setOf_navBarImage:(UIImage *)navBarImage {
  [self.navigationBar setBackgroundImage:navBarImage forBarMetrics:UIBarMetricsDefault];
}

- (UIImage *)of_navBarImage {
  return [self.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
}

- (void)setOf_navBarShadow:(BOOL)navBarShadow {
  if (self.of_navBarShadow != navBarShadow) {
    self.of_navBarImage = !navBarShadow ? [UIImage new] : nil;
    self.navigationBar.shadowImage = !navBarShadow ? [UIImage new] : nil;
  }
}

- (BOOL)of_navBarShadow {
  return self.navigationBar.shadowImage == nil;
}

- (void)setOf_navBarTransparent:(BOOL)navBarTransparent {
  if (self.of_navBarTransparent != navBarTransparent) {
    self.of_navBarImage = navBarTransparent ? [UIImage new] : nil;
    self.navigationBar.translucent = navBarTransparent;
  }
}

- (BOOL)of_navBarTransparent {
  return self.navigationBar.translucent && self.of_navBarImage == nil;
}

#pragma mark Custom Animations

- (UIImageView *)of_backgroundImageView {
  UIImageView *imageView = self.of_userInfo[kBackgroundImageView];
  if (!imageView) {
    imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view insertSubview:imageView atIndex:0];
    self.of_userInfo[kBackgroundImageView] = imageView;
  }
  return imageView;
}

- (void)setOf_backgroundImage:(UIImage *)backgroundImage {
  self.of_backgroundImageView.image = backgroundImage;
}

- (UIImage *)of_backgroundImage {
  return self.of_backgroundImageView.image;
}

- (void)setOf_fadeAnimation:(BOOL)fadeAnimation {
  if (fadeAnimation != self.of_fadeAnimation) {
    self.of_userInfo[kNavigationAnimator] = fadeAnimation ? [OFFadeNavigationAnimator new] : nil; // save strong reference
    self.delegate = self.of_userInfo[kNavigationAnimator];
    [self setupPopGesture];
  }
}

- (BOOL)of_fadeAnimation {
  return [self.delegate isKindOfClass:[OFFadeNavigationAnimator class]];
}

- (void)setOf_slideAnimation:(BOOL)slideAnimation {
  if (slideAnimation != self.of_slideAnimation) {
    OFSlideNavigationAnimator *slideNavigationAnimator = slideAnimation ? [OFSlideNavigationAnimator new] : nil;
    slideNavigationAnimator.interactivePopGestureOwner = self;
    self.of_userInfo[kNavigationAnimator] = slideNavigationAnimator; // save strong reference
    self.delegate = slideNavigationAnimator;
    [self setupPopGesture];
  }
}

- (BOOL)of_slideAnimation {
  return [self.delegate isKindOfClass:[OFSlideNavigationAnimator class]];
}

#pragma mark Pop Gesture

- (void)setOf_popGesture:(BOOL)popGesture {
  self.of_userInfo[kPopGestureEnabled] = @(popGesture);
  [self setupPopGesture];
}

- (BOOL)of_popGesture {
  return [self.of_userInfo[kPopGestureEnabled] boolValue];
}

- (void)setupPopGesture {
  if ([self.delegate isKindOfClass:[OFNavigationAnimator class]]) {
    OFNavigationAnimator *navigationAnimator = (OFNavigationAnimator *)self.delegate;
    navigationAnimator.interactivePopGestureOwner = self.of_popGesture ? self : nil;
    // disable system pop recognizer
    self.of_userInfo[kSystemPopGestureDelegate] = nil;
    self.interactivePopGestureRecognizer.delegate = nil;
    self.interactivePopGestureRecognizer.enabled = NO;
  } else {
    // setup system pop recognizer
    OFNavigationAnimator *systemPopGestureDelegate = self.of_popGesture ? [OFNavigationAnimator new] : nil;
    systemPopGestureDelegate.internalInteractivePopGestureOwner = self;
    self.of_userInfo[kSystemPopGestureDelegate] = systemPopGestureDelegate; // save strong reference
    self.interactivePopGestureRecognizer.delegate = systemPopGestureDelegate;
    self.interactivePopGestureRecognizer.enabled = systemPopGestureDelegate != nil;
  }
}

@end

#pragma mark -

@implementation OFNavigationAnimator

- (instancetype)init {
  self = [super init];
  if (self) {
    self.animationDuration = 0.3f;
  }
  return self;
}

- (void)dealloc {
  self.interactivePopGestureOwner = nil;
}

- (void)setInteractivePopGestureOwner:(UINavigationController *)interactivePopGestureOwner {
  if (self.internalInteractivePopGestureOwner != interactivePopGestureOwner) {
    if (interactivePopGestureOwner) {
      UIScreenEdgePanGestureRecognizer *recognizer = [UIScreenEdgePanGestureRecognizer new];
      recognizer.edges = UIRectEdgeLeft;
      recognizer.delegate = self;
      [recognizer addTarget:self action:@selector(handlePanGesture:)];
      [interactivePopGestureOwner.view addGestureRecognizer:recognizer];
      self.interactivePopGestureRecognizer = recognizer;
      self.internalInteractivePopGestureOwner = interactivePopGestureOwner;
    } else {
      [self.interactivePopGestureRecognizer.view removeGestureRecognizer:self.interactivePopGestureRecognizer];
      self.interactivePopGestureRecognizer = nil;
      self.internalInteractivePopGestureOwner = nil;
    }
  }
}

- (UINavigationController *)interactivePopGestureOwner {
  return self.internalInteractivePopGestureOwner;
}

#pragma mark UIGestureRecognizer Handler & Delegate

#define SWIPE_RIGHT_THRESHOLD 1000.0f

- (void)handlePanGesture:(UIScreenEdgePanGestureRecognizer *)recognizer {
  CGFloat progress = [recognizer translationInView:recognizer.view].x / recognizer.view.bounds.size.width;
  progress = MIN(1.0, MAX(0.0, progress));
  switch (recognizer.state) {
    case UIGestureRecognizerStateBegan: {
      self.interactiveTransition = [UIPercentDrivenInteractiveTransition new];
      self.interactiveTransition.completionCurve = UIViewAnimationCurveEaseOut;
      [self.interactivePopGestureOwner popViewControllerAnimated:YES];
      break;
    }
    case UIGestureRecognizerStateChanged: {
      [self.interactiveTransition updateInteractiveTransition:progress];
      break;
    }
    case UIGestureRecognizerStateEnded:
    case UIGestureRecognizerStateCancelled: {
      CGPoint vel = [recognizer velocityInView:recognizer.view];
      if (progress > 0.35 || vel.x > SWIPE_RIGHT_THRESHOLD) {
        self.interactiveTransition.completionSpeed = 2; // part of fix fast cancel transition animation
        [self.interactiveTransition finishInteractiveTransition];
      } else {
        [self.interactiveTransition cancelInteractiveTransition];
      }
      self.interactiveTransition = nil;
      break;
    }
    default:
      break;
  }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
  return self.internalInteractivePopGestureOwner.viewControllers.count > 1 && !self.internalInteractivePopGestureOwner.topViewController.of_appearing;
}

#pragma mark UINavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
  self.navigationOperation = operation;
  return self;
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController {
  return self.interactiveTransition;
}

#pragma mark UIViewControllerAnimatedTransitioning

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return self.animationDuration * ([transitionContext isInteractive] ? 3 : 1); // part of fix fast cancel transition animation
}

// override in subclass
- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {}

#pragma mark Delegate Forwarder

- (BOOL)respondsToSelector:(SEL)aSelector {
  return [super respondsToSelector:aSelector] || [self.forwardedDelegate respondsToSelector:aSelector];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
  return [super methodSignatureForSelector:aSelector] ?: [(id)self.forwardedDelegate methodSignatureForSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)invocation {
  if ([self.forwardedDelegate respondsToSelector:invocation.selector]) {
    [invocation invokeWithTarget:self.forwardedDelegate];
  }
}

@end

#pragma mark -

@implementation OFFadeNavigationAnimator

- (void)setInteractivePopGestureOwner:(UINavigationController *)interactivePopGestureOwner { // disable interactive gesture
  return;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  if (self.navigationOperation == UINavigationControllerOperationNone) {
    return;
  }
  UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  // Add the toView to the container
  toVC.view.frame = [transitionContext finalFrameForViewController:toVC];
  [[transitionContext containerView] insertSubview:toVC.view atIndex:0];
  // Animate
  [UIView animateWithDuration:[self transitionDuration:transitionContext]
                        delay:0
                      options:UIViewAnimationOptionCurveEaseInOut
                   animations:^{
                     fromVC.view.alpha = 0.0;
                   } completion:^(BOOL finished) {
                     if ([transitionContext transitionWasCancelled]) {
                       fromVC.view.alpha = 1.0;
                     } else {
                       // reset fromView to its original state
                       [fromVC.view removeFromSuperview];
                       fromVC.view.alpha = 1.0;
                     }
                     [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
                   }];
}

@end

#pragma mark -

@implementation OFSlideNavigationAnimator

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  if (self.navigationOperation == UINavigationControllerOperationNone) {
    return;
  }
  UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
  // Add the toView to the container
  toVC.view.frame = [transitionContext finalFrameForViewController:toVC];
  CGFloat toViewOffset = toVC.view.bounds.size.width * (self.navigationOperation == UINavigationControllerOperationPush ? 1 : -1);
  toVC.view.transform = CGAffineTransformMakeTranslation(toViewOffset, 0);
  [[transitionContext containerView] insertSubview:toVC.view atIndex:0];
  // Animate
  [UIView animateWithDuration:[self transitionDuration:transitionContext]
                        delay:0
                      options:[transitionContext isInteractive] ? UIViewAnimationOptionCurveLinear :UIViewAnimationOptionCurveEaseOut
                   animations:^{
                     fromVC.view.transform = CGAffineTransformMakeTranslation(-toViewOffset, 0);
                     toVC.view.transform = CGAffineTransformIdentity;
                   } completion:^(BOOL finished) {
                     if ([transitionContext transitionWasCancelled]) {
                       toVC.view.transform = CGAffineTransformIdentity;
                       fromVC.view.transform = CGAffineTransformIdentity;
                     } else {
                       // reset fromView to its original state
                       [fromVC.view removeFromSuperview];
                       fromVC.view.transform = CGAffineTransformIdentity;
                     }
                     [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
                   }];
}

@end
