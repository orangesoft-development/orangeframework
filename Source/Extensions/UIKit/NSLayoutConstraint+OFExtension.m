#import "NSLayoutConstraint+OFExtension.h"

@implementation NSLayoutConstraint (OFExtension)

- (void)setOf_constantInPixels:(NSInteger)constantPixel {
  self.constant = constantPixel / [UIScreen mainScreen].scale;
}

- (NSInteger)of_constantInPixels {
  return self.constant * [UIScreen mainScreen].scale;
}

#pragma mark Backward Compatibility

- (void)setOF_constantInPixels:(NSInteger)constantPixel {
  self.of_constantInPixels = constantPixel;
}

- (NSInteger)OF_constantInPixels {
  return self.of_constantInPixels;
}

@end
