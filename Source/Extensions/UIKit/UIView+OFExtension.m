#import "UIView+OFExtension.h"
#import "NSObject+OFExtension.h"

NSString *const kSeparatorView = @"of_kSeparatorView";

@implementation UIView (OFExtension)

- (void)setOf_cornerRadius:(CGFloat)cornerRadius {
  self.layer.cornerRadius = cornerRadius;
}

- (CGFloat)of_cornerRadius {
  return self.layer.cornerRadius;
}

- (void)setOf_borderWidth:(CGFloat)borderWidth {
  self.layer.borderWidth = borderWidth;
}

- (CGFloat)of_borderWidth {
  return self.layer.borderWidth;
}

- (void)setOf_borderColor:(UIColor *)borderColor {
  self.layer.borderColor = borderColor.CGColor;
}

- (UIColor *)of_borderColor {
  return self.layer.borderColor != nil ? [UIColor colorWithCGColor:self.layer.borderColor] : nil;
}

- (void)setOf_separatorColor:(UIColor *)separatorColor {
  [self _separatorView].backgroundColor = separatorColor;
}

- (UIColor *)of_separatorColor {
  return [self _separatorView].backgroundColor;
}

- (void)setOf_separatorLeftInset:(CGFloat)separatorLeftInset {
  CGRect rect = [self _separatorView].frame;
  rect.origin.x = separatorLeftInset;
  rect.size.width = self.bounds.size.width - separatorLeftInset - self.of_separatorRightInset;
  [self _separatorView].frame = rect;
}

- (CGFloat)of_separatorLeftInset {
  return CGRectGetMinX([self _separatorView].frame);
}

- (void)setOf_separatorRightInset:(CGFloat)separatorRightInset {
  CGRect rect = [self _separatorView].frame;
  rect.size.width = self.bounds.size.width - self.of_separatorLeftInset - separatorRightInset;
  [self _separatorView].frame = rect;
}


- (CGFloat)of_separatorRightInset {
  return CGRectGetMaxX([self _separatorView].frame);
}

#pragma mark Backward Compatibility

- (void)setOF_cornerRadius:(CGFloat)cornerRadius {
  self.of_cornerRadius = cornerRadius;
}

- (CGFloat)OF_cornerRadius {
  return self.of_cornerRadius;
}

- (void)setOF_borderWidth:(CGFloat)borderWidth {
  self.of_borderWidth = borderWidth;
}

- (CGFloat)OF_borderWidth {
  return self.of_borderWidth;
}

- (void)setOF_borderColor:(UIColor *)borderColor {
  self.of_borderColor = borderColor;
}

- (UIColor *)OF_borderColor {
  return self.of_borderColor;
}

#pragma mark Internal Helpers

- (UIView *)_separatorView {
  UIView *separator = self.of_userInfo[kSeparatorView];
  if (!separator) {
    CGFloat separatorHeight = 1.0f / [UIScreen mainScreen].scale;
    separator = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - separatorHeight, self.bounds.size.width, separatorHeight)];
    separator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self addSubview:separator];
    self.of_userInfo[kSeparatorView] = separator;
  }
  return separator;
}

@end
