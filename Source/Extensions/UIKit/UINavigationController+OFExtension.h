#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UINavigationController (OFExtension)

@property (nonatomic, strong, nullable) IBInspectable UIImage *of_navBarImage; // stretchable
@property (nonatomic) IBInspectable BOOL of_navBarShadow;
@property (nonatomic) IBInspectable BOOL of_navBarTransparent;

@property (nonatomic, strong, readonly) UIImageView *of_backgroundImageView;
@property (nonatomic, strong, nullable) IBInspectable UIImage *of_backgroundImage;
@property (nonatomic) IBInspectable BOOL of_fadeAnimation; // set OFFadeNavigationAnimator as delegate
@property (nonatomic) IBInspectable BOOL of_slideAnimation; // set OFSlideNavigationAnimator as delegate

@property (nonatomic) IBInspectable BOOL of_popGesture;

@end

@interface OFNavigationAnimator: NSObject <UINavigationControllerDelegate, UIViewControllerAnimatedTransitioning>
@property (nonatomic) NSTimeInterval animationDuration; // 0.3 by default
@property (nonatomic, weak, nullable) UINavigationController *interactivePopGestureOwner;
@property (nonatomic, weak, nullable) id<UINavigationControllerDelegate> forwardedDelegate;
@end

@interface OFFadeNavigationAnimator: OFNavigationAnimator
@end

@interface OFSlideNavigationAnimator: OFNavigationAnimator
@end

NS_ASSUME_NONNULL_END
