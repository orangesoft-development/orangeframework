#import <UIKit/UIKit.h>

@interface NSLayoutConstraint (OFExtension)

@property (nonatomic) IBInspectable NSInteger of_constantInPixels; // for storyboard

@end
