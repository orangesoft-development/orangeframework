#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (OFExtension)

+ (instancetype)of_imageWithColor:(UIColor *)color size:(CGSize)size NS_SWIFT_NAME(init(of_color:size:));
+ (instancetype)of_imageWithColor:(UIColor *)color size:(CGSize)size transparentInsets:(UIEdgeInsets)insets NS_SWIFT_NAME(init(of_color:size:transparentInsets:));

+ (instancetype)of_imageFromView:(UIView *)view NS_SWIFT_NAME(init(of_view:));

- (UIImage *)of_imageScaledToSize:(CGSize)size;
- (UIImage *)of_imageScaledToFitSize:(CGSize)size;
- (UIImage *)of_imageScaledToFillSize:(CGSize)size;

- (UIImage *)of_imageRoundedWithCornerRadius:(CGFloat)cornerRadius;
- (UIImage *)of_imageRoundedIntoCircle;

- (UIImage *)of_imageWithNormalizedOrientation; // fix orientation

@property (nonatomic, readonly) BOOL of_isOpaque;

@end

NS_ASSUME_NONNULL_END
