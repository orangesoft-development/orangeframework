#import "UIImage+OFExtension.h"
// scaling and rounding based on https://github.com/Alamofire/AlamofireImage/blob/master/Source/UIImage%2BAlamofireImage.swift

@implementation UIImage (OFExtension)

+ (instancetype)of_imageWithColor:(UIColor *)color size:(CGSize)size {
  return [self of_imageWithColor:color size:size transparentInsets:UIEdgeInsetsZero];
}

+ (instancetype)of_imageWithColor:(UIColor *)color size:(CGSize)size transparentInsets:(UIEdgeInsets)insets {
  CGRect coloredRect = CGRectMake(insets.left, insets.top, size.width - insets.left - insets.right, size.height - insets.top - insets.bottom);
  UIGraphicsBeginImageContextWithOptions(size, UIEdgeInsetsEqualToEdgeInsets(insets, UIEdgeInsetsZero), 0);
  [color setFill];
  UIRectFill(coloredRect);
  UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return resultImage;
}

+ (instancetype)of_imageFromView:(UIView *)view {
  UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0);
  [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:YES];
  UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return resultImage;
}

- (UIImage *)of_imageScaledToSize:(CGSize)size {
  UIGraphicsBeginImageContextWithOptions(size, self.of_isOpaque, 0);
  [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
  UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return scaledImage;
}

- (UIImage *)of_imageScaledToFitSize:(CGSize)size {
  CGFloat imageAspectRatio = self.size.width / self.size.height;
  CGFloat canvasAspectRatio = size.width / size.height;
  CGFloat resizeFactor;
  if (imageAspectRatio > canvasAspectRatio) {
    resizeFactor = size.width / self.size.width;
  } else {
    resizeFactor = size.height / self.size.height;
  }
  CGSize scaledSize = CGSizeMake(self.size.width * resizeFactor, self.size.height * resizeFactor);
  CGPoint origin = CGPointMake((size.width - scaledSize.width) / 2.0, (size.height - scaledSize.height) / 2.0);
  UIGraphicsBeginImageContextWithOptions(size, NO, 0);
  [self drawInRect:CGRectMake(origin.x, origin.y, scaledSize.width, scaledSize.height)];
  UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return scaledImage;
}

- (UIImage *)of_imageScaledToFillSize:(CGSize)size {
  CGFloat imageAspectRatio = self.size.width / self.size.height;
  CGFloat canvasAspectRatio = size.width / size.height;
  CGFloat resizeFactor;
  if (imageAspectRatio > canvasAspectRatio) {
    resizeFactor = size.height / self.size.height;
  } else {
    resizeFactor = size.width / self.size.width;
  }
  CGSize scaledSize = CGSizeMake(self.size.width * resizeFactor, self.size.height * resizeFactor);
  CGPoint origin = CGPointMake((size.width - scaledSize.width) / 2.0, (size.height - scaledSize.height) / 2.0);
  UIGraphicsBeginImageContextWithOptions(size, self.of_isOpaque, 0);
  [self drawInRect:CGRectMake(origin.x, origin.y, scaledSize.width, scaledSize.height)];
  UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return scaledImage;
}

- (UIImage *)of_imageRoundedWithCornerRadius:(CGFloat)cornerRadius {
  UIGraphicsBeginImageContextWithOptions(self.size, NO, 0);
  CGRect bounds = CGRectMake(0, 0, self.size.width, self.size.height);
  UIBezierPath *clippingPath = [UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:cornerRadius];
  [clippingPath addClip];
  [self drawInRect:bounds];
  UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return roundedImage;
}

- (UIImage *)of_imageRoundedIntoCircle {
  CGFloat radius = MIN(self.size.width, self.size.height) / 2.0f;
  UIImage *squareImage = self;
  if (self.size.width != self.size.height) {
    CGFloat squareDimension = MIN(self.size.width, self.size.height);
    CGSize squareSize = CGSizeMake(squareDimension, squareDimension);
    squareImage = [self of_imageScaledToFillSize:squareSize];
  }
  return [squareImage of_imageRoundedWithCornerRadius:radius];
}

- (UIImage *)of_imageWithNormalizedOrientation {
  if (self.imageOrientation == UIImageOrientationUp) {
    return self;
  }
  UIGraphicsBeginImageContextWithOptions(self.size, self.of_isOpaque, self.scale);
  [self drawInRect:CGRectMake(0, 0, self.size.width, self.size.height)];
  UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return normalizedImage;
}

- (BOOL)of_isOpaque {
  CGImageAlphaInfo alphaInfo = CGImageGetAlphaInfo(self.CGImage);
  return (alphaInfo == kCGImageAlphaFirst ||
          alphaInfo == kCGImageAlphaLast ||
          alphaInfo == kCGImageAlphaPremultipliedFirst ||
          alphaInfo == kCGImageAlphaPremultipliedLast);
}

@end
