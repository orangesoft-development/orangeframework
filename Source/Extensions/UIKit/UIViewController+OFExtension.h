#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^OFSeguePreparationBlock)(UIStoryboardSegue *segue) NS_SWIFT_NAME(SeguePreparation);

@interface UIViewController (OFExtension)

+ (nullable UIViewController *)of_topViewController;

@property (nonatomic, readonly) BOOL of_appearing; // YES on willAppear and NO on didAppear
@property (nonatomic, readonly) BOOL of_disappearing; // YES on willDisappear and NO on didDisappear

- (IBAction)of_popAction:(nullable id)sender; // for storyboard
- (IBAction)of_dismissAction:(nullable id)sender; // for storyboard

- (void)of_performSegueWithIdentifier:(NSString *)identifier preparation:(OFSeguePreparationBlock)preparation;

@end

NS_ASSUME_NONNULL_END
