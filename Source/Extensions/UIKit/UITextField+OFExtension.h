#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextField (OFExtension)

@property (nonatomic, strong, nullable) IBInspectable UIColor *of_placeholderColor;
@property (nonatomic, strong, nullable) UIBarButtonItem *of_inputAccessoryLeftButton;
@property (nonatomic, strong, nullable) UIBarButtonItem *of_inputAccessoryRightButton;
@property (nonatomic) UIEdgeInsets of_textInsets;

@end

NS_ASSUME_NONNULL_END
