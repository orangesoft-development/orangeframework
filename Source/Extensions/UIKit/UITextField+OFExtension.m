#import "UITextField+OFExtension.h"
#import "NSObject+OFExtension.h"

NSString *const kPlaceholderColor = @"of_kPlaceholderColor";
NSString *const kLeftToolbarButton = @"of_kLeftToolbarButton";
NSString *const kRightToolbarButton = @"of_kRightToolbarButton";
NSString *const kTextInsets = @"kTextInsets";

@implementation UITextField (OFExtension)

+ (void)load {
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    [self of_swizzleMethod:@selector(setPlaceholder:) withMethod:@selector(of_setPlaceholder:)];
    [self of_swizzleMethod:@selector(textRectForBounds:) withMethod:@selector(of_textRectForBounds:)];
    [self of_swizzleMethod:@selector(editingRectForBounds:) withMethod:@selector(of_editingRectForBounds:)];
  });
}

- (void)setOf_placeholderColor:(UIColor *)placeholderColor {
  self.of_userInfo[kPlaceholderColor] = placeholderColor;
  self.placeholder = self.placeholder; // update color
}

- (UIColor *)of_placeholderColor {
  return self.of_userInfo[kPlaceholderColor];
}

- (void)setOf_inputAccessoryLeftButton:(UIBarButtonItem *)inputAccessoryLeftButton {
  self.of_userInfo[kLeftToolbarButton] = inputAccessoryLeftButton;
  [self updateToolbar];
}

- (UIBarButtonItem *)of_inputAccessoryLeftButton {
  return self.of_userInfo[kLeftToolbarButton];
}

- (void)setOf_inputAccessoryRightButton:(UIBarButtonItem *)inputAccessoryRightButton {
  self.of_userInfo[kRightToolbarButton] = inputAccessoryRightButton;
  [self updateToolbar];
}

- (UIBarButtonItem *)of_inputAccessoryRightButton {
  return self.of_userInfo[kRightToolbarButton];
}

- (void)setOf_textInsets:(UIEdgeInsets)textInsets {
  NSValue *value = [NSValue value:&textInsets withObjCType:@encode(UIEdgeInsets)];
  self.of_userInfo[kTextInsets] = value;
  [self setNeedsLayout];
}

- (UIEdgeInsets)of_textInsets {
  NSValue *value = self.of_userInfo[kTextInsets];
  if (value) {
    UIEdgeInsets textInsets;
    [value getValue:&textInsets];
    return textInsets;
  } else {
    return UIEdgeInsetsZero;
  }
}

#pragma mark Swizzled

- (void)of_setPlaceholder:(NSString *)placeholder {
  if (self.of_placeholderColor) {
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholder
                                                                 attributes:@{NSForegroundColorAttributeName: self.of_placeholderColor}];
  } else {
    [self of_setPlaceholder:placeholder]; // call original exchanged method
  }
}

- (CGRect)of_textRectForBounds:(CGRect)bounds {
  CGRect rect = [self of_textRectForBounds:bounds]; // call original exchanged method
  return [self changedRect:rect withInsets:self.of_textInsets];
}

- (CGRect)of_editingRectForBounds:(CGRect)bounds {
  CGRect rect = [self of_editingRectForBounds:bounds]; // call original exchanged method
  return [self changedRect:rect withInsets:self.of_textInsets];
}

#pragma mark Internal Helpers

- (void)updateToolbar {
  if (self.of_inputAccessoryLeftButton || self.of_inputAccessoryRightButton) {
    UIToolbar *toolbar = [UIToolbar new];
    toolbar.barStyle = (self.keyboardAppearance == UIKeyboardAppearanceDark) ? UIBarStyleBlack : UIBarStyleDefault;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSMutableArray *items = [NSMutableArray new];
    if (self.of_inputAccessoryLeftButton) {
      [items addObject:self.of_inputAccessoryLeftButton];
    }
    [items addObject:space];
    if (self.of_inputAccessoryRightButton) {
      [items addObject:self.of_inputAccessoryRightButton];
    }
    toolbar.items = items;
    [toolbar sizeToFit];
    self.inputAccessoryView = toolbar;
  } else {
    self.inputAccessoryView = nil;
  }
}

- (CGRect)changedRect:(CGRect)rect withInsets:(UIEdgeInsets)insets {
  rect.origin.x += insets.left;
  rect.size.width -= (insets.left + insets.right);
  rect.origin.y += insets.top;
  rect.size.height -= (insets.top + insets.bottom);
  return rect;
}

@end

