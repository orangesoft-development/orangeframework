#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (OFExtension)

@property (nonatomic) IBInspectable CGFloat of_cornerRadius;
@property (nonatomic) IBInspectable CGFloat of_borderWidth;
@property (nonatomic, strong, nullable) IBInspectable UIColor *of_borderColor;

@property (nonatomic, strong, nullable) IBInspectable UIColor *of_separatorColor;
@property (nonatomic) IBInspectable CGFloat of_separatorLeftInset;
@property (nonatomic) IBInspectable CGFloat of_separatorRightInset;

@end

NS_ASSUME_NONNULL_END
