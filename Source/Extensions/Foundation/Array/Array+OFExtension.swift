
public extension Array {
  
  func of_safeElement(at index: Int) -> Element? {
    return index < count ? self[index] : nil
  }
  
}

public extension Array where Element: Equatable {
  
  mutating func of_remove(_ element: Element) {
    for index in (0..<count).reversed() where self[index] == element {
      self.remove(at: index)
    }
  }
  
}
