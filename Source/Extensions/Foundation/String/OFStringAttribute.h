#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface OFStringAttribute : NSObject

@property (strong, nonatomic, readonly) NSString *key;
@property (strong, nonatomic, readonly) id value;


- (instancetype)initWithValue:(id)value andKey:(NSString *)key;

+ (OFStringAttribute *)font:(UIFont *)font;
+ (OFStringAttribute *)paragraphStyle:(NSParagraphStyle *)paragraphStyle;
+ (OFStringAttribute *)foregroundColor:(UIColor *)foregroundColor;
+ (OFStringAttribute *)backgroundColor:(UIColor *)backgroundColor;
+ (OFStringAttribute *)strokeColor:(UIColor *)strokeColor;
+ (OFStringAttribute *)strokeWidth:(NSNumber *)strokeWidth;
+ (OFStringAttribute *)setLigature:(NSNumber *)ligature;
+ (OFStringAttribute *)kern:(NSNumber *)kern;
+ (OFStringAttribute *)strikethroughStyle:(NSNumber *)strikethroughStyle;
+ (OFStringAttribute *)underlineStyle:(NSInteger)underlineStyle;
+ (OFStringAttribute *)shadow:(NSShadow *)shadow;
+ (OFStringAttribute *)underlineColor:(UIColor *)underlineColor;
+ (OFStringAttribute *)textEffect:(NSString *)textEffect;
+ (OFStringAttribute *)link:(NSString *)link;
+ (OFStringAttribute *)baselineOffset:(NSNumber *)baselineOffset;
+ (OFStringAttribute *)attachment:(NSTextAttachment *)attachment;
+ (OFStringAttribute *)obliqueness:(NSNumber *)obliqueness;
+ (OFStringAttribute *)expansion:(NSNumber *)expansion;
+ (OFStringAttribute *)writingDirection:(NSArray<NSNumber *> *)writingDirection;
+ (OFStringAttribute *)verticalGlyphForm:(NSNumber *)verticalGlyphForm;

@end
