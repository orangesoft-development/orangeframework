import Foundation

public extension String {
  
  // MARK: - To Number
  
  func of_toInt() -> Int? {
    return NumberFormatter().number(from: self)?.intValue
  }
  
  func of_toDouble() -> Double? {
    let formatter = NumberFormatter()
    formatter.decimalSeparator = ","
    if let value = formatter.number(from: self)?.doubleValue {
      return value
    }
    formatter.decimalSeparator = "."
    return formatter.number(from: self)?.doubleValue
  }
  
  init(of_int int: Int, groupingSeparator separator: String) {
    let formatter = NumberFormatter()
    formatter.groupingSeparator = separator
    formatter.numberStyle = .decimal
    self = formatter.string(from: int as NSNumber) ?? ""
  }
  
  // MARK: - Reading Time
  
  /// speed = words per minute. default speed is 200
  func of_readingTime(speed: UInt = 200) -> TimeInterval {
    return (self as NSString).of_readingTime(withSpeed: speed)
  }
  
  // MARK: - Regex
  
  func of_match(regex pattern: String) -> Bool {
    return (self as NSString).of_matchRegex(pattern)
  }
  
  func of_firstMatch(ofRegex pattern: String) -> String? {
    return (self as NSString).of_firstMatch(ofRegex: pattern)
  }
  
  func of_allMatches(ofRegex pattern: String) -> [String] {
    return (self as NSString).of_allMatches(ofRegex: pattern)
  }
  
  func of_matchEmailPattern() -> Bool {
    return (self as NSString).of_matchEmailPattern()
  }
  
  func of_removeSpaces() -> String {
    return (self as NSString).removeSpaces()
  }
}
