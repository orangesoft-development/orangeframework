#import "NSMutableAttributedString+OFExtension.h"

@implementation NSMutableAttributedString (OFExtension)

- (instancetype)initWithString:(NSString *)string andAttributes:(NSArray<OFStringAttribute *> *)attributes {
  self = [super init];
  if (self) {
    NSMutableDictionary *stringAttributes = [NSMutableDictionary new];
    for (OFStringAttribute *stringAttribute in attributes) {
      [stringAttributes setValue:stringAttribute.value forKey:stringAttribute.key];
    }
    return [self initWithString:string attributes:stringAttributes];
  }
  return self;
}

- (NSMutableAttributedString *)of_appendString:(NSString *)string withAttributes:(NSArray<OFStringAttribute *> *)attributes {
  NSMutableDictionary *stringAttributes = [NSMutableDictionary new];
  for (OFStringAttribute *stringAttribute in attributes) {
    [stringAttributes setValue:stringAttribute.value forKey:stringAttribute.key];
  }
  NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string attributes:stringAttributes];
  [self appendAttributedString: attributedString];
  return self;
}

- (NSMutableAttributedString *)of_addAtributes:(NSArray<OFStringAttribute *> *)attributes {
  NSMutableDictionary *stringAttributes = [NSMutableDictionary new];
  for (OFStringAttribute *stringAttribute in attributes) {
    [stringAttributes setValue:stringAttribute.value forKey:stringAttribute.key];
  }
  [self addAttributes:stringAttributes range:NSMakeRange(0, self.string.length)];
  return self;
}


@end
