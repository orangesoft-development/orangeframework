#import "OFStringAttribute.h"

@interface OFStringAttribute ()

@end

@implementation OFStringAttribute

- (instancetype)initWithValue:(id)value andKey:(NSString *)key {
  self = [super init];
  if (self) {
    _value = value;
    _key = key;
  }
  return self;
}

+ (OFStringAttribute *)font:(UIFont *)font {
  return [[OFStringAttribute alloc] initWithValue:font andKey:NSFontAttributeName];
}

+ (OFStringAttribute *)paragraphStyle:(NSParagraphStyle *)paragraphStyle {
  return [[OFStringAttribute alloc] initWithValue:paragraphStyle andKey:NSParagraphStyleAttributeName];
}

+ (OFStringAttribute *)foregroundColor:(UIColor *)foregroundColor {
   return [[OFStringAttribute alloc] initWithValue:foregroundColor andKey:NSForegroundColorAttributeName];
}

+ (OFStringAttribute *)backgroundColor:(UIColor *)backgroundColor {
   return [[OFStringAttribute alloc] initWithValue:backgroundColor andKey:NSBackgroundColorAttributeName];
}

+ (OFStringAttribute *)strokeColor:(UIColor *)strokeColor {
  return [[OFStringAttribute alloc] initWithValue:strokeColor andKey:NSStrokeColorAttributeName];
}

+ (OFStringAttribute *)strokeWidth:(NSNumber *)strokeWidth {
  return [[OFStringAttribute alloc] initWithValue:strokeWidth andKey:NSStrokeWidthAttributeName];
}

+ (OFStringAttribute *)setLigature:(NSNumber *)ligature {
  return [[OFStringAttribute alloc] initWithValue:ligature andKey:NSLigatureAttributeName];
}

+ (OFStringAttribute *)kern:(NSNumber *)kern {
  return [[OFStringAttribute alloc] initWithValue:kern andKey:NSKernAttributeName];
}

+ (OFStringAttribute *)strikethroughStyle:(NSNumber *)strikethroughStyle {
  return [[OFStringAttribute alloc] initWithValue:strikethroughStyle andKey:NSStrikethroughStyleAttributeName];
}

+ (OFStringAttribute *)underlineStyle:(NSInteger)underlineStyle {
  return [[OFStringAttribute alloc] initWithValue:@(underlineStyle) andKey:NSUnderlineStyleAttributeName];
}

+ (OFStringAttribute *)shadow:(NSShadow *)shadow {
  return [[OFStringAttribute alloc] initWithValue:shadow andKey:NSShadowAttributeName];
}

+ (OFStringAttribute *)underlineColor:(UIColor *)underlineColor {
  return [[OFStringAttribute alloc] initWithValue:underlineColor andKey:NSUnderlineColorAttributeName];
}

+ (OFStringAttribute *)textEffect:(NSString *)textEffect {
  return [[OFStringAttribute alloc] initWithValue:textEffect andKey:NSTextEffectAttributeName];
}

+ (OFStringAttribute *)link:(NSString *)link {
  return [[OFStringAttribute alloc] initWithValue:link andKey:NSLinkAttributeName];
}

+ (OFStringAttribute *)baselineOffset:(NSNumber *)baselineOffset {
  return [[OFStringAttribute alloc] initWithValue:baselineOffset andKey:NSBaselineOffsetAttributeName];
}

+ (OFStringAttribute *)attachment:(NSTextAttachment *)attachment {
  return [[OFStringAttribute alloc] initWithValue:attachment andKey:NSAttachmentAttributeName];
}

+ (OFStringAttribute *)obliqueness:(NSNumber *)obliqueness {
  return [[OFStringAttribute alloc] initWithValue:obliqueness andKey:NSObliquenessAttributeName];
}

+ (OFStringAttribute *)expansion:(NSNumber *)expansion {
  return [[OFStringAttribute alloc] initWithValue:expansion andKey:NSExpansionAttributeName];
}

+ (OFStringAttribute *)writingDirection:(NSArray<NSNumber *> *)writingDirection {
  return [[OFStringAttribute alloc] initWithValue:writingDirection andKey:NSWritingDirectionAttributeName];
}

+ (OFStringAttribute *)verticalGlyphForm:(NSNumber *)verticalGlyphForm {
  return [[OFStringAttribute alloc] initWithValue:verticalGlyphForm andKey:NSVerticalGlyphFormAttributeName];
}

@end
