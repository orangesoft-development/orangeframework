#import <Foundation/Foundation.h>
#import "OFStringAttribute.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSMutableAttributedString (OFExtension)

- (instancetype)initWithString:(NSString *)string andAttributes:(NSArray<OFStringAttribute *> *)attributes NS_SWIFT_NAME(init(of_string:with:));
- (NSMutableAttributedString *)of_appendString:(NSString *)string withAttributes:(NSArray<OFStringAttribute *> *)attributes;
- (NSMutableAttributedString *)of_addAtributes:(NSArray<OFStringAttribute *> *)attributes;

@end

NS_ASSUME_NONNULL_END
