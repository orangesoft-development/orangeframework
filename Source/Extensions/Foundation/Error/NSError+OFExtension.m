#import "NSError+OFExtension.h"

@implementation NSError (OFExtension)

+ (instancetype)of_errorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description {
  return [self errorWithDomain:domain code:code userInfo:@{ NSLocalizedDescriptionKey: description }];
}

+ (instancetype)of_errorWithDomainObject:(id)domainObject description:(NSString *)description {
  return [self of_errorWithDomainObject:domainObject code:0 description:description];
}

+ (instancetype)of_errorWithDomainObject:(id)domainObject code:(NSInteger)code description:(NSString *)description {
  return [self of_errorWithDomain:NSStringFromClass([domainObject class]) code:code description:description];
}

@end
