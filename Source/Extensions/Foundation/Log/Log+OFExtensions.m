#import "Log+OFExtensions.h"

static OFLogFilterBlock _filterBlock = nil;
#ifdef DEBUG
static const BOOL _defaultFilterResult = YES;
#else
static const BOOL _defaultFilterResult = NO;
#endif

void OFLogFilter(OFLogFilterBlock filter) {
  _filterBlock = [filter copy];
}

void __OFLog(OFLogLazyObject value, OFLogLevel level, NSString *function, NSString *file, unsigned int line) {
  if (!_filterBlock && !_defaultFilterResult) {
    return;
  }
  __block NSString *formattedMessage = nil;
  OFLogLazyFormattedMessage lazyFormattedMessage = ^NSString * _Nonnull {
    if (!formattedMessage) {
      NSDateFormatter *dateFormatter = [NSDateFormatter new];
      dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"ddMMyyyy HHmmssSSS" options:0 locale:nil];
      NSString *timeString = [dateFormatter stringFromDate:[NSDate date]];
      NSString *levelString;
      switch (level) {
        case OFLogLevelDefault:
          levelString = @"";
          break;
        case OFLogLevelInfo:
          levelString = @" [INFO]";
          break;
        case OFLogLevelWarning:
          levelString = @" [WARNING]";
          break;
        case OFLogLevelError:
          levelString = @" [ERROR]";
      }
      NSString *filename = [file lastPathComponent];
      NSString *message = [value() description];
      formattedMessage = [NSString stringWithFormat:@"%@%@ %@:%d (%@): %@", timeString, levelString, filename, line, function, message];
    }
    return formattedMessage;
  };
  if (!_filterBlock || (_filterBlock && _filterBlock(lazyFormattedMessage, value, level, function, file, line))) {
    printf("%s\n", [lazyFormattedMessage() UTF8String]);
  }
}
