import Foundation

/// setup which message should print, by default print all in DEBUG and nothing otherwise
public func of_printFilter(_ filter: @escaping LogFilter) {
  __OFLogFilter(filter)
}

public func of_print(_ value: @escaping @autoclosure () -> Any?, level: LogLevel = .default, _ function: String = #function, _ file: String = #file, _ line: UInt32 = #line) {
  __OFLog(value, level, function, file, line)
}
