#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, OFLogLevel)
{
  OFLogLevelDefault,
  OFLogLevelInfo,
  OFLogLevelWarning,
  OFLogLevelError
} NS_SWIFT_NAME(LogLevel);

typedef NSString * _Nonnull (^OFLogLazyFormattedMessage)(void) NS_SWIFT_NAME(LogLazyFormattedMessage);
typedef id _Nullable (^OFLogLazyObject)(void) NS_SWIFT_NAME(LogLazyValue);
typedef BOOL (^OFLogFilterBlock)(OFLogLazyFormattedMessage formattedMessage, OFLogLazyObject value, OFLogLevel level, NSString *function, NSString *file, unsigned int line) NS_SWIFT_NAME(LogFilter);

FOUNDATION_EXPORT void __OFLog(OFLogLazyObject value, OFLogLevel level, NSString *function, NSString *file, unsigned int line);
FOUNDATION_EXPORT void OFLogFilter(OFLogFilterBlock filter) NS_REFINED_FOR_SWIFT; // setup which message should print, by default print all in DEBUG and nothing otherwise

#define OFLog(object) __OFLog(^id{ return object; }, OFLogLevelDefault, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)
#define OFLogInfo(object) __OFLog(^id{ return object; }, OFLogLevelInfo, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)
#define OFLogWarning(object) __OFLog(^id{ return object; }, OFLogLevelWarning, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)
#define OFLogError(object) __OFLog(^id{ return object; }, OFLogLevelError, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)

#define OFLogF(format, ...) __OFLog(^id{ return [NSString stringWithFormat:format,##__VA_ARGS__]; }, OFLogLevelDefault, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)
#define OFLogInfoF(format, ...) __OFLog(^id{ return [NSString stringWithFormat:format,##__VA_ARGS__]; }, OFLogLevelInfo, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)
#define OFLogWarningF(format, ...) __OFLog(^id{ return [NSString stringWithFormat:format,##__VA_ARGS__]; }, OFLogLevelWarning, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)
#define OFLogErrorF(format, ...) __OFLog(^id{ return [NSString stringWithFormat:format,##__VA_ARGS__]; }, OFLogLevelError, [NSString stringWithUTF8String:__func__], [NSString stringWithUTF8String:__FILE__], __LINE__)

NS_ASSUME_NONNULL_END
