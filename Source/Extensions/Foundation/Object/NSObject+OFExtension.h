#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (OFExtension)

+ (BOOL)of_swizzleMethod:(SEL)originalSelector withMethod:(SEL)swizzledSelector;
+ (BOOL)of_swizzleClassMethod:(SEL)originalSelector withClassMethod:(SEL)swizzledSelector;

- (BOOL)of_swizzleMethod:(SEL)originalSelector withMethod:(SEL)swizzledSelector; // swizzle only for current object

@property (nonatomic, strong, readonly) NSMutableDictionary *of_userInfo;

@end

NS_ASSUME_NONNULL_END
