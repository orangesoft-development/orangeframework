import Foundation

public extension String {
  
  /// localized string for current locale from .stringdict (firstly) and .strings files
  func of_localized(_ args: CVarArg...) -> String {
    let step1 = Bundle.main.localizedString(forKey: self, value: "", table: nil)
    let step2 = String(format: step1, locale: Locale.current, arguments: args)
    return step2
  }
  
}
