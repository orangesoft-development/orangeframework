#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDate (OFExtension)

+ (nullable instancetype)of_dateFromString:(NSString *)dateString format:(NSString *)format timezone:(nullable NSTimeZone *)timezone;

+ (nullable instancetype)of_dateFromComponents:(NSDateComponents *)components;
- (nullable instancetype)of_dateByChangeComponents:(void (^)(NSDateComponents *components))changeBlock; // components with year, month, day, hour, minute, second, nanosecond, weekday, weekOfMonth, weekOfYear, timeZone
- (NSDateComponents *)of_components; // year, month, day, hour, minute, second, nanosecond, weekday, weekOfMonth, weekOfYear, timeZone

- (NSString *)of_dateStringWithFormat:(NSString *)format timezone:(nullable NSTimeZone *)timezone;
- (NSString *)of_dateStringWithFormatTemplate:(NSString *)formatTemplate timezone:(nullable NSTimeZone *)timezone;
- (NSString *)of_dateStringWithDateStyle:(NSDateFormatterStyle)dateStyle timeStyle:(NSDateFormatterStyle)timeStyle timezone:(nullable NSTimeZone *)timezone;

- (BOOL)of_isEqualToDateIgnoringTime:(NSDate *)otherDate;

@end

NS_ASSUME_NONNULL_END
