import Foundation

public extension Date {
  
  init?(of_string string: String, format: String, timezone: TimeZone? = nil) {
    guard let date = NSDate.of_date(from: string, format: format, timezone: timezone) else {
      return nil
    }
    self.init(timeIntervalSince1970: date.timeIntervalSince1970)
  }
  
  init?(of_components components: DateComponents) {
    guard let date = NSDate.of_date(from: components) else {
      return nil
    }
    self.init(timeIntervalSince1970: date.timeIntervalSince1970)
  }
  
  /// components - year, month, day, hour, minute, second, nanosecond, weekday, weekOfMonth, weekOfYear, timeZone
  func of_changed(_ change: (_ components: inout DateComponents) -> Void) -> Date? {
    var selfComponents = of_components()
    change(&selfComponents)
    return Date(of_components: selfComponents)
  }
  
  /// components - year, month, day, hour, minute, second, nanosecond, weekday, weekOfMonth, weekOfYear, timeZone
  func of_components() -> DateComponents {
    return (self as NSDate).of_components()
  }
  
  func of_toString(format: String, timezone: TimeZone? = nil) -> String {
    return (self as NSDate).of_dateString(withFormat: format, timezone: timezone)
  }
  
  func of_toString(formatTemplate: String, timezone: TimeZone? = nil) -> String {
    return (self as NSDate).of_dateString(withFormatTemplate: formatTemplate, timezone: timezone)
  }
  
  func of_toString(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style, timezone: TimeZone? = nil) -> String {
    return (self as NSDate).of_dateString(withDateStyle: dateStyle, time: timeStyle, timezone: timezone)
  }
  
  func of_isEqualIgnoringTime(to date: Date) -> Bool {
    return (self as NSDate).of_isEqual(toDateIgnoringTime: date)
  }
  
}
