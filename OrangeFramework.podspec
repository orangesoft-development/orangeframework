#
# Be sure to run `pod lib lint OrangeFramework.podspec' to ensure this is a
# valid spec before submitting.
#

Pod::Spec.new do |s|
  s.name             = "OrangeFramework"
  s.version          = "3.0.2"
  s.summary          = "Powerful framework aimed at fast and high quality developing mobile apps on Objective-C and Swift."
  s.description      = <<-DESC
                       OrangeFramework is a collection of helpers for fast and high quality developing mobile apps on Objective-C and Swift.
                       DESC
  s.homepage         = "https://bitbucket.org/orangesoft-development/orangeframework"
  s.license          = 'MIT'
  s.author           = { "Roman Kulesha" => "kulesha.r@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/orangesoft-development/orangeframework.git", :tag => s.version.to_s }
  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.xcconfig = { 'SWIFT_INSTALL_OBJC_HEADER' => 'NO', 'ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES' => 'NO' }

  s.subspec 'Utilities' do |ss|
    ss.source_files = 'Source/Utilities/**/*.{swift,h,m}'
  end

  s.subspec 'Extensions' do |ss|
    ss.source_files = 'Source/Extensions/**/*.{swift,h,m}'
  end

  s.subspec 'Views' do |ss|
    ss.source_files = 'Source/Views/**/*.{swift,h,m}'
  end

  s.subspec 'Segues' do |ss|
    ss.source_files = 'Source/Segues/**/*.{swift,h,m}'
  end


end
